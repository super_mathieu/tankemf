#-*- coding:utf-8 -*-
from Tkinter import Tk
from tkFileDialog import askopenfilename
import csv
import cx_Oracle

#Cr�ation d'une connexion
try:
    maConnexion = cx_Oracle.connect("e1163569", "b", "10.57.4.60/DECINFO.edu")
    print ("Connection effectu�e")
except cx_Oracle.DatabaseError as e:
    error, = e.args
    print ("Erreur de connection!!!")
    print error.code #Code de l'erreur pour rechercher sur Google
    print error.message #Donne une description du message
    print error.context #Donne la ligne/le caractere/la fonction ou se situe le probleme

def executer(curseur, strCommande):
   try:
       curseur.execute(strCommande)
   except cx_Oracle.DatabaseError as e:
       error, = e.args
       print ("Erreur de connection!!!")
       print error.code #Code de l'erreur pour rechercher sur Google
       print error.message #Donne une description du message
       print error.context #Donne la ligne/le caractere/la fonction ou se situe le probleme

monCurseur = maConnexion.cursor()
cmd_Lecture = "select * from description_balance"
executer(monCurseur, cmd_Lecture)
requete = monCurseur.fetchall()
resultat = []

for r in requete:
    resultat.append(r)

monCurseur.close()
maConnexion.close()

def choisirFichierCSV():  
    #Emp�che une fen�tre Tkinter de s'ouvrir
    Tk().withdraw()
    nomFichier = askopenfilename(filetypes = [("CSV Files", "*.csv"),("All Filles", "*")], defaultextension="*.csv")
    return nomFichier

nomFichier = choisirFichierCSV()

with open(nomFichier, "w") as fichier:
    writer = csv.writer(fichier, delimiter = ';', quotechar = '"')
    for i in resultat:
        #Va �crire la description et les valeurs du joueur.
        writer.writerow([i[1],i[5]])