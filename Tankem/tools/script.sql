DROP TABLE description_balance;
DROP TABLE description_balance_1;

CREATE TABLE description_balance(
	id number generated always as identity(start with 1 increment by 1) PRIMARY KEY,
	Description   VARCHAR2(100) 	  NOT NULL,
	Min           FLOAT    			NOT NULL,
	Max           FLOAT    			NOT NULL,
	Defaut        FLOAT    			NOT NULL,
	Valeur        FLOAT    			NOT NULL
);

CREATE TABLE description_balance_1(
	id number generated always as identity(start with 1 increment by 1) PRIMARY KEY,
	Description   	VARCHAR2(100) 		NOT NULL,
	Max_caractere   FLOAT 				NOT NULL,
	Defaut 			VARCHAR2(100) 		NOT NULL
);

INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Vitesse des chars',4.00,12.00,7.00,7.00);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Vitesse de rotation des chars',1000.00,2000.00,1500.00,1500.00);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Points de vie des chars',100.00,2000.00,200.00,200.00);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Temps du mouvement des blocs anim�s',0.20,2.00,0.08,0.08);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Canon - Vitesse balle',4.00,30.00,14.00,14.00);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Canon - Temps de recharge',0.2,10.00,1.20,1.20);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Mitraillette - Vitesse balle',4.00,30.00,18.00,18.00);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Mitraillette - Temps de recharge',0.20,10.00,0.40),0.40;
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Grenade - Vitesse initiale balle',10.00,25.00,16.00,16.00);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Grenade - Temps de recharge',0.20,10.00,0.80,0.80);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Shotgun - Vitesse balle',4.00,30.00,13.00,13.00);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Shotgun - Temps de recharge',0.20,10.00,1.80,1.80);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Shotgun - Ouverture du fusil',0.10,1.50,0.40,0.40);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Piege - Vitesse balle',0.20,4.00,1.00,1.00);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Piege - Temps de recharge',0.20,10.00,0.80,0.80);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Missile guid� - Vitesse guid�e balle',20.00,40.00,30.00,30.00);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Missile guid� - Temps de recharge',0.20,10.00,3.00,3.00);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Spring - Vitesse initiale du saut',6.00,20.00,10.00,10.00);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Spring - Temps de recharge',0.20,10.00,0.50,0.50);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Grosseur de lexplosion des balles',1.00,30.00,8.00,8.00);

INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Message daccueil - dur�e',1.00,10.00,3.00,3.00);
INSERT INTO description_balance(Description,Min,Max,Defaut, Valeur) VALUES ('Message compte � rebour - dur�e',0.00,10.00,3.00,3.00);

INSERT INTO description_balance_1(Description,Max_caractere,Defaut) VALUES ('Message daccueil - contenu',60,'X');
INSERT INTO description_balance_1(Description,Max_caractere,Defaut) VALUES ('Message - signal d�but de partie - contenu',50,'X');
INSERT INTO description_balance_1(Description,Max_caractere,Defaut) VALUES ('Message fin de partie - contenu',70,'X');