#-*- coding:utf-8 -*-
from Tkinter import Tk
from tkFileDialog import askopenfilename
import csv
import cx_Oracle

def choisirFichierCSV():
    #Empêche une fenêtre Tkinter de s'ouvrir
    Tk().withdraw()
    nomFichier = askopenfilename(filetypes = [("CSV Files", "*.csv"),("All Filles", "*")], defaultextension="*.csv")
    return nomFichier

nomFichier = choisirFichierCSV()
requete = []

with open(nomFichier, "r") as fichier:
	index = 0
	print("Ouverture du fichier")
	#Préciser le dialect est important
	reader = csv.reader(fichier,delimiter = ';', quotechar = '"')
	for row in reader:
		for cell in row:
			if index % 2 == 1:
				requete.append(cell)
			index+=1

try:
    maConnexion = cx_Oracle.connect("e1163569", "b", "10.57.4.60/DECINFO.edu")
    print ("Connection effectuée")
except cx_Oracle.DatabaseError as e:
    error, = e.args
    print ("Erreur de connection!!!")
    print error.code #Code de l'erreur pour rechercher sur Google
    print error.message #Donne une description du message
    print error.context #Donne la ligne/le caractere/la fonction ou se situe le probleme

def executer(curseur, strCommande):
   try:
       curseur.execute(strCommande)
   except cx_Oracle.DatabaseError as e:
       error, = e.args
       print ("Erreur de connection!!!")
       print error.code #Code de l'erreur pour rechercher sur Google
       print error.message #Donne une description du message
       print error.context #Donne la ligne/le caractere/la fonction ou se situe le probleme

index = 1
monCurseur = maConnexion.cursor()

for i in requete:
	try:
		i = float(i)
		cmd_Update = "UPDATE description_balance SET Valeur=%s WHERE id=%s" % (i,index,)
		executer(monCurseur, cmd_Update)
		index+=1
	except:
		pass

executer(monCurseur, "commit")

monCurseur.close()
maConnexion.close()