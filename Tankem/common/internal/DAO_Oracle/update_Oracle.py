#-*- coding:utf-8 -*-
from Tkinter import Tk
from tkFileDialog import askopenfilename
import csv
import cx_Oracle

def choisirFichierCSV():
    #Empêche une fenêtre Tkinter de s'ouvrir
    Tk().withdraw()
    nomFichier = askopenfilename(filetypes = [("CSV Files", "*.csv"),("All Filles", "*")], defaultextension="*.csv")
    return nomFichier

nomFichier = choisirFichierCSV()
requete = []
requete_description = []

with open(nomFichier, "r") as fichier:
	index = 0
	print("Ouverture du fichier")
	#Préciser le dialect est important
	reader = csv.reader(fichier,delimiter = ';', quotechar = '"')
	for row in reader:
		for cell in row:
			if index % 2 == 1:
				requete.append(cell)
			else:
				requete_description.append(cell)
			index+=1
#print(requete_description)
try:
    maConnexion = cx_Oracle.connect("e0442162", "adsi72cq72", "10.57.4.60/DECINFO.edu")
    print ("Connection effectuée")
except cx_Oracle.DatabaseError as e:
    error, = e.args
    print ("Erreur de connection!!!")
    print error.code #Code de l'erreur pour rechercher sur Google
    print error.message #Donne une description du message
    print error.context #Donne la ligne/le caractere/la fonction ou se situe le probleme

def executer(curseur, strCommande):
   try:
       curseur.execute(strCommande)
   except cx_Oracle.DatabaseError as e:
       error, = e.args
       print ("Erreur de connection!!!")
       print error.code #Code de l'erreur pour rechercher sur Google
       print error.message #Donne une description du message
       print error.context #Donne la ligne/le caractere/la fonction ou se situe le probleme

index = 1
monCurseur = maConnexion.cursor()


# *********************************************************
# *********** 	VÉRIFICATION DES BORNES *******************
# *********************************************************

valMin = "select MIN from DESCRIPTION_BALANCE"
valMax = "select MAX from DESCRIPTION_BALANCE"
v_min = []
v_max = []
executer(monCurseur,valMin)
requete1 = monCurseur.fetchall()

for r in requete1:
    v_min.append(r[0])

executer(monCurseur,valMax)
requete2 = monCurseur.fetchall()

for r in requete2:
    v_max.append(r[0])

k=0
for i in range(len(requete)/2+1):
	requete[k] = float(requete[k])
	if(v_min[i] <= requete[k] <= v_max[i]):
		print(u"OK!!")
	else:
		print(u"Erreur, la donnée ' "+requete_description[k]+" ' est hors des bornes permises.")
		print(u"Valeur minimum permise : "+str(v_min[i]))
		print(u"Valeur maximum permise : "+str(v_max[i]))
		print(u"Valeur choisie : "+str(requete[k]))
	k+=2

# *********************************************************


for i in requete:
	try:
		i = float(i)
		cmd_Update = "UPDATE description_balance SET Valeur=%s WHERE id=%s" % (i,index,)
		executer(monCurseur,cmd_Update)
		index+=1
	except:
		pass

executer(monCurseur, "commit")

monCurseur.close()
maConnexion.close()