## -*- coding: utf-8 -*-
#Ajout des chemins vers les librarires
from util import inclureCheminCegep
#from menu_options import *
#from Ecriture_CSV_Vers_Tankem import *
import sys
print(sys.path)

#Importe la configuration de notre jeu
from panda3d.core import loadPrcFile
loadPrcFile("config/ConfigTankem.prc")

#Module de Panda3D
from direct.showbase.ShowBase import ShowBase

#Modules internes
from gameLogic import GameLogic
from interface import InterfaceMenuPrincipal

class Tankem(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        self.demarrer()

    def demarrer(self):
        self.gameLogic = GameLogic(self)
        #Commenter/décommenter la ligne de votre choix pour démarrer le jeu
        #Démarre dans le menu
        self.menuPrincipal = InterfaceMenuPrincipal()
        #Démarre directement dans le jeu
        #messenger.send("DemarrerPartie")

# class Menu():
# 	def __init__(self, parent):
# 		self.bk_text = "Tankem - 2016"
# 		self.textObject = OnscreenText(text = self.bk_text, pos = (0,0.85), 
# 		scale = 0.07,fg=(1,0.5,0.5,1),align=TextNode.ACenter,mayChange=1)
# 		self.valeurs = []
# 		self.ecriture = EcritureTankem("")
# 	def itemSel(arg):
# 		if(arg == "Démarrer une partie"):
# 			self.gameLogic = GameLogic(parent)
# 			self.gameLogic.startGame()
# 			self.menu.hide()
# 		elif(arg == "Options"):
# 			self.ecriture.lireFichier()
# 			self.ecriture.convertirFloat()
# 			self.valeurs = self.ecriture.getValeursJoueur()
# 			#self.testValeur()
# 			#self.options = Options(self.menu)
# 			#self.menu.hide()
# 			#self.options.afficherOptions()
# 		elif(arg == "Quitter"):
# 			exit()

# 		self.menu = DirectOptionMenu(text='Démarrer une partie',scale=0.1,items=['Démarrer une partie','Options','Quitter','Menu Tankem'],initialitem=3,highlightColor=(0.65,0.65,0.65,1),command=itemSel, textMayChange=1)

app = Tankem()
app.run()