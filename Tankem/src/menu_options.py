# -*- coding: utf-8 -*-
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
#from Ecriture_CSV_Vers_Tankem import *
 
class Options():
	def __init__(self,parent):
		self.parent = parent
		self.composantes = []
		self.label = []
		self.valeurDefault = [7.0,1500.0, 200.0, 0.8, 14.0, 1.2, 18.0, 0.4, 16.0, 0.8, 13.0, 1.8, 0.4, 1.0, 0.8, 30.0, 3.0, 10.0, 0.5, 8.0, 'Bonjour!', 3.0, 3.0, 'Début de la partie','Fin de la partie']
		self.valeurJoueur = []

	def cacher(self):
		for i in self.composantes:
			i.hide()
		for j in self.label:
			j.hide()
		
		self.parent.show()
	
	def remplirDefault(self):
		index = 0
		for i in self.composantes:
			i.enterText(str(self.valeurDefault[index]))
			index += 1

	def receuillirDonnee(self):
		for i in self.composantes:
			self.valeurJoueur.append(i.get())
			
		for j in self.valeurJoueur:
			print j
		
		self.cacher()

	def afficherOptions(self):
		#Tank
		#Vitesse des chars
		textObjectTank1 = OnscreenText(text = 'Vitesse des chars', pos = (-1.3,0.88), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		vitesseTank = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -1.3, 0,0.8 ) )
		self.label.append(textObjectTank1)
		self.composantes.append(vitesseTank)
		#Vitesse de rotation des chars
		textObjectTank2 = OnscreenText(text = 'Rotation des chars', pos = (-1.3,0.63), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		vitesseRotation = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -1.3, 0, 0.55 ) )
		self.label.append(textObjectTank2)
		self.composantes.append(vitesseRotation)
		#Points de vie des chars
		textObjectTank3 = OnscreenText(text = 'Points de vie des chars', pos = (-1.3,0.38), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		hpTank = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -1.3, 0, 0.3 ) )
		self.label.append(textObjectTank3)
		self.composantes.append(hpTank)

		# #Bloc
		#Temps du mouvements des blocs animés
		textObjectBloc = OnscreenText(text = 'Fréquence des blocs animés', pos = (-1.3,0.13), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		tempsBloc = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -1.3, 0, 0.05 ) )
		self.label.append(textObjectBloc)
		self.composantes.append(tempsBloc)

		# #Canon
		#Canon - Vitesse balle
		textObjectCanon1 = OnscreenText(text = 'Canon - Vitesse balle', pos = (-1.3,-0.12), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		vitesseCanon = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -1.3, 0, -0.20 ) )
		self.label.append(textObjectCanon1)
		self.composantes.append(vitesseCanon)
		#Canon - Recharge
		textObjectCanon2 = OnscreenText(text = 'Canon - Recharge', pos = (-1.3,-0.37), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		rechargeCanon = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -1.3, 0, -0.45 ) )
		self.label.append(textObjectCanon2)
		self.composantes.append(rechargeCanon)

		# #Mitraillette
		#Mitraillette - Vitesse balle
		textObjectMitraillette1 = OnscreenText(text = 'Mitraillette - Vitesse balle', pos = (-1.3,-0.62), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		vitesseMitraillette = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -1.3, 0, -0.70 ) )
		self.label.append(textObjectMitraillette1)
		self.composantes.append(vitesseMitraillette)
		#Mitraillette - Recharge
		textObjectMitraillette2 = OnscreenText(text = 'Mitraillette - Recharge', pos = (-1.3,-0.88), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		rechargeMitraillette = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -1.3, 0, -0.95 ) )
		self.label.append(textObjectMitraillette2)
		self.composantes.append(rechargeMitraillette)

		# #Grenade
		#Grenade - Vitesse initiale balle
		textObjectGrenade1 = OnscreenText(text = 'Grenade - Vitesse des balles', pos = (-0.6,0.88), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		rechargeGrenade = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -0.6, 0, 0.8 ) )
		self.label.append(textObjectGrenade1)
		self.composantes.append(rechargeGrenade)
		#Grenade - Recharge
		textObjectGrenade2 = OnscreenText(text = 'Grenade - Recharge', pos = (-0.6,0.63), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		rechargeGrenade = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -0.6, 0, 0.55 ) )
		self.label.append(textObjectGrenade2)
		self.composantes.append(rechargeGrenade)

		# #Shotgun
		#Shotgun - Vitesse balle
		textObjectShotGun1 = OnscreenText(text = 'Shotgun - Vitesse balle', pos = (-0.6,0.38), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		vitesseShotgun = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -0.6, 0, 0.3 ) )
		self.label.append(textObjectShotGun1)
		self.composantes.append(vitesseShotgun)
		#Shotgun - Recharge
		textObjectShotGun2 = OnscreenText(text = 'Shotgun - Recharge', pos = (-0.6,0.13), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		rechargeShotgun = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -0.6, 0, 0.05 ) )
		self.label.append(textObjectShotGun2)
		self.composantes.append(rechargeShotgun)
		#Shotgun - Ouverture du fusil
		textObjectShotGun3 = OnscreenText(text = 'Shotgun - Ouverture du fusil', pos = (-0.6,-0.12), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		ouvertureShotgun = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -0.6, 0, -0.20 ) )
		self.label.append(textObjectShotGun3)
		self.composantes.append(ouvertureShotgun)

		# #Piege
		#Piege - Vitesse balle
		Piege1 = OnscreenText(text = 'Piege - Vitesse balle', pos = (-0.6,-0.37), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		vitessePiege = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -0.6, 0, -0.45 ) )
		self.label.append(Piege1)
		self.composantes.append(vitessePiege)
		#Piege - Recharge
		Piege2 = OnscreenText(text = 'Piege - Recharge', pos = (-0.6,-0.62), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		rechargePiege = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -0.6, 0, -0.70 ) )
		self.label.append(Piege2)
		self.composantes.append(rechargePiege)

		# #Missile guidé
		#Missile guidé - Vitesse guidée balle
		textObjectMissible1 = OnscreenText(text = 'Missile guidé - Vitesse', pos = (-0.6,-0.87), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		vitesseMissile = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( -0.6, 0, -0.95 ) )
		self.label.append(textObjectMissible1)
		self.composantes.append(vitesseMissile)
		#Missile guidé - Recharge
		textObjectMissible2 = OnscreenText(text = 'Missile guidé - Vitesse', pos = (0.1,0.88), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		rechargeMissile = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( 0.1, 0, 0.8 ) )
		self.label.append(textObjectMissible2)
		self.composantes.append(rechargeMissile)

		# #Spring
		#Spring - Vitesse initiale du saut
		textObjectSpring1 = OnscreenText(text = 'Spring - initiale du saut', pos = (0.1,0.63), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		vitesseSpring = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( 0.1, 0, 0.55 ) )
		self.label.append(textObjectSpring1)
		self.composantes.append(vitesseSpring)
		#Spring - Recharge
		textObjectSpring2 = OnscreenText(text = 'Spring - Recharge', pos = (0.1,0.38), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		rechargeSpring = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( 0.1, 0, 0.3 ) )
		self.label.append(textObjectSpring2)
		self.composantes.append(rechargeSpring)

		#Rayon Explosion
		#Rayon d'explosion des balles
		textObjectRayon1 = OnscreenText(text = 'Rayon Explosion', pos = (0.1,0.13), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		rayonExplosion = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( 0.1, 0, 0.05 ) )
		self.label.append(textObjectRayon1)
		self.composantes.append(rayonExplosion)

		#Messages
		#Message d'acceuil - Contenu
		textObjectMessages1 = OnscreenText(text = "Message d\'acceuil - Contenu", pos = (0.1,-0.12), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		msgAcceuilDuree = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( 0.1, 0, -0.20 ) )
		self.label.append(textObjectMessages1)
		self.composantes.append(msgAcceuilDuree)
		#Message d'acceuil - Durée
		textObjectMessages2 = OnscreenText(text = 'Message d\'acceuil - Durée', pos = (0.1,-0.37), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		msgDebutPartie = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( 0.1, 0, -0.45 ) )
		self.label.append(textObjectMessages2)
		self.composantes.append(msgDebutPartie)
		#Message compte à rebour - Durée
		textObjectMessages3 = OnscreenText(text = 'Message compte à rebour - Durée', pos = (0.1,-0.62), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		msgFinPartie = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( 0.1, 0, -0.70 ) )
		self.label.append(textObjectMessages3)
		self.composantes.append(msgFinPartie)
		#Message - signal début de partie
		textObjectMessages4 = OnscreenText(text = 'Message - signal début de partie', pos = (0.1,-0.87), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		msgAcceuil = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( 0.1, 0, -0.95 ) )
		self.label.append(textObjectMessages4)
		self.composantes.append(msgAcceuil)
		#Message fin de partie
		textObjectMessages5 = OnscreenText(text = 'Message fin de partie', pos = (0.8,0.88), scale = 0.05,fg=(1,0.5,0.5,1),align=TextNode.ALeft)
		msgRebour = DirectEntry(text = "" ,scale=.05,initialText="", numLines = 1,focus=1,pos=( 0.8, 0, 0.8 ) )
		self.label.append(textObjectMessages5)
		self.composantes.append(msgRebour)
		 
		bOk = DirectButton(text = "OK", scale=.07, pos=(0.8,0,0.05), command=self.receuillirDonnee)
		bDefault = DirectButton(text = "Défaut", scale=.07, pos=(1.1,0,0.05), command=self.remplirDefault)

		self.label.append(bOk)
		self.label.append(bDefault)
	
if __name__ == "__main__":
    objName = Options()
    objName.afficherOptions() 